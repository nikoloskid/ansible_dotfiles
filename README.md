# ansible_home-infra_provision
Ansible collection for provisioning a server / Desktop PC

This collection includes multiple roles
- Set up Catppuccin theme on KDE
- Set up .dotfiles
    - Catppuccin Custom Theme
    - neofetch
    - Wallpapers
    - Tray icons
    - Copies tabliss theme
- Installs docker and docker-compose
    - Arch Linux support
    - Debian support
> **To Do**
> yay and pacman packages roles

## Usage

1. Clone this repository to your local machine
2. Set up virtual python environment
    - `python3 -m venv .venv`
    - `. .venv/bin/activate` to activate the virtual environment
    - `pip3 install -r requirements.txt`
    - `ansible-galaxy collection install -r collections/requirements.yml`
    - `ansible-playbook main.yml -e "hosts=" --tags`
        - Remember to change host to localhost or all for Vagrant boxes
    - Testing roles in Vagrant
        - `vagrant up arch` to deploy Arch Linux VM
        - `vagrant up debian` to deploy Debian VM
        - `vagrant provision arch/debian` to provision the playbook
